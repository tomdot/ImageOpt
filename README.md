# Image Optimiser

An image optimisation script that watches a folder for new images, sends them to an API, and returns new images to the
downloads folder with reduced file sizes.

## Installation
Download the script to any directory, and from a Terminal window, navigate to the working directory and run the ```requirements.txt``` file:

```bash
pip install -r /path/to/requirements.txt
```
Once installed, run the ```program.py``` file via the Terminal:

```bash
python3 program.py
```

Note that because Python's ```pathlib``` is being used, you'll need to run Python 3.7+.

## Usage
The default watched folder is the desktop. It carries out non-recursive checks, and the program doesn't currently check whether there are images already there.

Also, you'll need to create a ```secrets.py``` file in the main directory, and add your TinyPNG API Key to it as a variable named ```api_key```.

## Roadmap
The ```program.py``` file shows the general roadmap for development in the form of TODOs.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPLv3.0](https://choosealicense.com/licenses/gpl-3.0/)
