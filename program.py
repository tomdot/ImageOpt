# References:
# [1] https://medium.com/@ageitgey/python-3-quick-tip-the-easy-way-to-deal-with-file-paths-on-windows-mac-and-linux-11a072b58d5f
# [2] https://stackoverflow.com/questions/19273210/how-to-open-a-file-on-mac-osx-10-8-2-in-python/19273589#19273589
# [3] https://shortpixel.com/api-docs#post, https://tinypng.com/developers/reference/python
# [4] https://stackoverflow.com/questions/58158649/when-trying-to-import-pandas-get-process-finished-with-exit-code-132-interrupt
# [5] https://stackoverflow.com/questions/32802148/illegal-instruction-4-when-importing-python-pandas
#
# TODO: Check for images on observer start and on_modified (isfile?)
# TODO: Resizing procedure.
# TODO: Implement GUI.
# TODO: BUG: Error when moving file out of working dir during optimisation and folder creation.
# TODO: "Done" message.


import os
import secrets
import shutil

from pathlib import Path, PurePath
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import tinify as tf

from gui import ToolBar


class MonitorFolderChanges(FileSystemEventHandler):
    def __init__(self, working_path):
        self.path = working_path

    def on_modified(self, event):
        file_exts = [".jpg", ".jpeg", ".png"]

        images = []

        for file in list(Path(self.path).glob("*.*")):
            file = PurePath(file.name)

            full_path = PurePath(f"{self.path}/{file.name}")

            if full_path.suffix.lower() in file_exts:
                images.append(file)
                print(f"Image found: {file}.")

                posted_image = CallApi(full_path, file)
                get_tinified_file = posted_image.compress_image()

                posted_image.move_file(full_path, self.path, file)

                set_new_dir = CreateNewFolder(self.path, get_tinified_file, images)
                new_dir = set_new_dir.create_folder()
                set_new_dir.download_image(new_dir)
            else:
                continue


class ObserverLoop:
    def __init__(self):
        self.folder = Path("~/Desktop")
        self.working_path = self.folder.expanduser()
        self.event_handler = MonitorFolderChanges(self.working_path)

    def start_loop(self):
        tf.key = secrets.api_key
        observer = Observer()

        observer.schedule(self.event_handler, self.working_path, recursive=False)
        observer.start()

        try:
            while observer.is_alive():
                observer.join(1)
        except KeyboardInterrupt:
            observer.stop()

        observer.join()


class CallApi:
    def __init__(self, path, file):
        self.path = path
        self.file = file

    def compress_image(self):
        print(f"Optimising {self.file}...")
        source = tf.tinify.from_file(self.path)
        return source

    def move_file(self, full_path, working_folder, file):
        folder = Path(f"{working_folder}/Unoptimised Images")
        new_path = f"{folder}/{file}"

        if not folder.exists():
            folder.mkdir()

        shutil.move(full_path, new_path)
        print(f"Moved original image to: {folder}")


class CreateNewFolder:
    def __init__(self, path, source, image_list):
        self.path = path
        self.source = source
        self.images = image_list

    def create_folder(self):
        folder_path = Path(f"{self.path}/Tinified Images")
        if not folder_path.exists():
            folder_path.mkdir()
        return folder_path

    def download_image(self, new_dir):
        for _ in self.images:
            pop_img = self.images.pop()
            self.source.to_file(f"{new_dir}/{pop_img}")

            print(f"Optimised and available at: {new_dir}/\n")


if __name__ == "__main__":
    # ToolBar(name='Image Optimiser', icon='assets/cam.png').run()
    launch = ObserverLoop()
    launch.start_loop()
