import rumps


class ToolBar(rumps.App):
    @rumps.clicked("About")
    def prefs(self, _):
        rumps.alert("""
            Image Optimiser 1.0 © Tom Rankin 2020
            https://github.com/thomasdot/image-optimiser
            
            Image credit: [OpenIcons](https://pixabay.com/users/OpenIcons-28911/)
            """)
